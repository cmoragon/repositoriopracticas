
package com.how2java;

/*
   Copyright [2022] [Carlos Moragón Corella]

   Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @version java 11.0.12 2021-07-20 LTS.
 *
 * @author Carlos Moragón Corella
 *
 * @see App es una clase para llamar al método de la clase CalculadoraDistancias, llamado calcularDistancia, e imprimiendo por pantalla el resultado.
 * Está puesto con un ejemplo predefinido por el autor.
 */

public class App {

	public static void main(String[] args) {
		System.out.println(CalculadoraDistancias.calcularDistancia("Juan","Charlie"));
	}
}
