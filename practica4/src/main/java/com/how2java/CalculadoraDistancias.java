package com.how2java;

import java.util.function.BiFunction;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.function.Consumer;

/*
   Copyright [2022] [Carlos Moragón Corella]

   Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @author Carlos Moragón Corella
 * @see CalculadoraDistancias contiene el método calcularDistancia, nos calculada la distancia de edición de 2 cadenas, y el método inicializacion que nos inicializa la matriz distancias.
 *
 * Calcula la distancia de edición, tal y como la expone
 * Jurafsky entre las cadenas ` s1` y ` s2`.
 */
public class CalculadoraDistancias{
	/**
	 * @return Variable int, la cual la minima distancia de edición para que ambas palabras sean iguales.
	 *
	 * @param s1 Es un string sobre que utilizaremos para calcular la distancia de los cambios entre s1 y s2 para que sean identicos.
	 * @param s2 Es un string sobre que utilizaremos para calcular la distancia de los cambios entre s1 y s2 para que sean identicos.
	 */
	public static int calcularDistancia(String s1, String s2){
		int cambiar = 0;

		BiFunction<Integer, Integer, Integer> minimo = (x,y) -> {
			if(x < y){
				return x;
			}else if(x > y){
				return y;
			}else return x;
		};

		s1 = " " + s1;
		s2 = " " + s2;

		String[] cadena = s2.split("");
		String[] patron = s1.split("");

		int[][] distancias = new int[cadena.length][patron.length];

		inicializacion(distancias,cadena.length, patron.length);

		for(int i = 1; i < cadena.length; i++){
			for(int j = 1; j < patron.length; j++){
				if(patron[j].equalsIgnoreCase(cadena[j])){
					cambiar = distancias[i-1][j-1];
				}else{
					cambiar = 2+distancias[i-1][j-1];
				}
				distancias[i][j] = minimo.apply(minimo.apply(1 + distancias[i-1][j], 1+distancias[i][j-1]),cambiar);
			}
		}

		return distancias[cadena.length -1][patron.length -1];

	}

	/**
	 * @param x es array bidimensional de int's, el cual es la matriz distancias sin inicializar
	 * @param cadena es un parametro de tipo int que nos da la longitud del string s2.
	 * @param patron es un parametro de tipo int que nos da la longitud del string s1.
	 */
	private static void inicializacion(int[][] x, int cadena, int patron){
		for(int i = 0; i < cadena; i++){
			x[i][0] = i;
		}
		for(int j = 0; j < patron; j++){
			x[0][j] = j;
		}
	}
}
