package com.how2java;

import static org.junit.Assert.*;
import org.junit.Test;

/*
   Copyright [2022] [Carlos Moragón Corella]

   Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @author Carlos Moragón Corella
 *
 * @see CalculadoraDistanciasTest se llaman a pruebas del código para comprobar la funcionalidad del mismo. Se realizan con ayuda de las librerias junit.
 */
public class CalculadoraDistanciasTest{

	@Test
	public void pruebaSimple(){
		int distancia = CalculadoraDistancias.calcularDistancia("pepe","pepapig");
		assertEquals(distancia, 5);
	}

	@Test
	public void pruebaVacio1(){
		int distancia = CalculadoraDistancias.calcularDistancia("","pepapig");
		assertEquals(distancia, 7);
	}

	@Test
	public void pruebaVacio2(){
		int distancia = CalculadoraDistancias.calcularDistancia("pepe","");
		assertEquals(distancia, 4);
	}

	@Test
	public void pruebaIguales(){
		int distancia = CalculadoraDistancias.calcularDistancia("pepe","pepe");
		assertEquals(distancia, 0);
	}
}
