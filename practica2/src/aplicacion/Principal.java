package aplicacion;

import mates.Matematicas;

/*
Copyright [2022] [Carlos Moragón Corella]

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @author anonimo.
 * @author Carlos Moragón.
 *
 * @see en esta clase se inicializa el método de clase Matematicas, llamado generarNumeroPiIterativo(), con un 0 para definir el valor inicial de dardos dentro de la diana, con el 1 para el término inicial y con el valor args[0], que es el número total de dardos lanzados.
 *
 */

public class Principal{
	public static void main(String[] args){
		System.out.println("El número PI es " +
				Matematicas.generarNumeroPiIterativo(0, 1, Integer.parseInt(args[0])));
	}
}
