package mates;

import java.lang.Math;
import java.lang.StackOverflowError;

/*
   Copyright [2022] [Carlos Moragón Corella]

   Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @version java 11.0.12 2021-07-20 LTS
 *
 * @author Carlos Moragon Corella.
 *
 * @see en esta clase se realiza el método de montecarlo de forma recursiva.
 */

public class Matematicas{
	/**
	 * @param al método se le pasa tres parámetro de tipo long. El parámetro d, es el número de dardos que caen dentro de la diana, el parámetro i es una variable que utilizamos para avanzar en la recursividad, y el parámetro pasos es el número de dados totales, que se han lanzado.
	 *
	 * @return el método generarNumeroPiIterativo() devuelve un double, el cual se debe aproximar al numero pi: 3,1416...
	 */

	public static double generarNumeroPiIterativo(long d, long i, long pasos){
		if(i > pasos){
			return (4*d)/(double)pasos;
		}else{
			double x = Math.random();
			double y = Math.random();
			if(Math.pow(x,2) + Math.pow(y,2) <= 1){
				d++;
			}
			return generarNumeroPiIterativo(d, i + 1, pasos);
		}
	}
}
