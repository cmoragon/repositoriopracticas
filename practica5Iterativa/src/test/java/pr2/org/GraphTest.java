package pr2.org;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GraphTest {
    @Test
    public void shortestPathFindsAPath() throws Exception {
        System.out.println("\nTest shortestPathFindsAPath");
        System.out.println("----------------------------");
        // We build the graph
        Graph<Integer> g = new Graph<>();
        g.addVertex(1);
        g.addVertex(2);
        g.addVertex(3);
        g.addVertex(4);
        g.addVertex(5);

        g.addEdge(1, 2);
        g.addEdge(1, 5);
        g.addEdge(2, 3);
        g.addEdge(3, 4);
        g.addEdge(5, 4);
        // We build the expected path
        List<Integer> expectedPath = new ArrayList<>();
        expectedPath.add(1);
        expectedPath.add(5);
        expectedPath.add(4);
        //We check if the returned path is equal to the expected one.
        System.out.println( g.shortestPath(1, 4));
        assertEquals(expectedPath, g.shortestPath(1, 4));
    }

    @Test
    public void pruebaExtra() throws Exception {
        System.out.println("\nTest shortestPathFindsAPath");
        System.out.println("----------------------------");
        // We build the graph
        Graph<Integer> g = new Graph<>();
        g.addVertex(1);
        g.addVertex(2);
        g.addVertex(3);
        g.addVertex(4);
        g.addVertex(5);

        g.addEdge(1, 2);
        g.addEdge(1, 5);
        g.addEdge(2, 3);
        g.addEdge(3, 4);
        g.addEdge(5, 4);
        // We build the expected path
        List<Integer> expectedPath = new ArrayList<>();
        expectedPath.add(1);
        expectedPath.add(2);
        expectedPath.add(3);
        //We check if the returned path is equal to the expected one.
        System.out.println( g.shortestPath(1, 3));
        assertEquals(expectedPath, g.shortestPath(1, 3));
    }


}
