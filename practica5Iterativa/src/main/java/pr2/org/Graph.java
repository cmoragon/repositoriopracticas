package pr2.org;

import java.util.*;
import java.util.stream.Collectors;

/*
 * Copyright [2022] [Carlos Moragón Corella]

 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

public class Graph<V> {
	//Lista de adyacencia.
	private Map<V, Set<V>> adjacencyList = new HashMap<>();

	private final int infinito = 2147483647;

	/**
	 * Añ ade el vé rtice ` v` al grafo.
	 * <p>
	 * 4
	 *
	 * @ param v vé rtice a añ adir.
	 * @ return ` true` si no estaba anteriormente y ` false` en caso
	 * contrario.
	 */
	public boolean addVertex(V v) {
		if (!adjacencyList.containsKey(v)) {
			adjacencyList.put(v, new HashSet<V>());
			return true;
		} else return false;
	}


	/**
	 * Añ ade un arco entre los vé rtices ` v1` y ` v2` al grafo. En
	 * caso de que no exista alguno de los vé rtices, lo añ ade
	 * tambié n.
	 *
	 * @ param v1 el origen del arco.
	 * @ param v2 el destino del arco.
	 * @ return ` true` si no existí a el arco y ` false` en caso contrario.
	 */
	public boolean addEdge(V v1, V v2) {
		if (!adjacencyList.get(v1).contains(v2) && !adjacencyList.get(v2).contains(v1)) {
			adjacencyList.get(v1).add(v2);
			adjacencyList.get(v2).add(v1);
			return true;
		} else return false;
	}


	/**
	 * Obtiene el conjunto de vé rtices adyacentes a ` v`.
	 *
	 * @ param v vé rtice del que se obtienen los adyacentes.
	 * @ return conjunto de vé rtices adyacentes.
	 */
	public Set<V> obtainAdjacents(V v) throws Exception {
		return adjacencyList.get(v);
	}


	/**
	 * Comprueba si el grafo contiene el vé rtice dado.
	 *
	 * @ param v vé rtice para el que se realiza la comprobació n.
	 * @ return ` true` si ` v` es un vé rtice del grafo.
	 */
	public boolean containsVertex(V v) {
		return adjacencyList.keySet().contains(v);
	}


	/**
	 * Mé todo ` toString()` reescrito para la clase ` Grafo. java`.
	 *
	 * @ return una cadena de caracteres con la lista de
	 * adyacencia.
	 */

	@Override
	public String toString() {
		StringBuilder mensaje = new StringBuilder();
		for (V v : adjacencyList.keySet()) {
			mensaje.append(v.toString() + ": ");
			for (V conect : adjacencyList.get(v)) {
				mensaje.append(conect.toString() + " ");
			}
		}
		return mensaje.toString();
	}


	/**
	 * Obtiene, en caso de que exista, el camino má s corto entre
	 * ` v1` y ` v2`. En caso contrario, devuelve ` null`.
	 *
	 * @ param v1 el vé rtice origen.
	 * @ param v2 el vé rtice destino.
	 * @ return lista con la secuencia de vé rtices del camino má s corto
	 * entre ` v1` y ` v2`
	 */


	public List<Integer> shortestPath(V v1, V v2) {
		//Lista solución, con el camino má s corto.
		List<Integer> solucion = new ArrayList<>();

		//Conjunto de vé rtices intermedios visitados.
		Set<Integer> conjunto = new HashSet<>();

		//Pila para almacenar los vé rtices intermedios, del camino final.
		Stack<Integer> pila = new Stack();

		//Array para almacenar la distancia más corta del vertice v1 a todos los demás.
		int[] distancias = new int[adjacencyList.size()];

		//Array para almacenar el vé rtice anterior a cada vé rtice.
		int[] anterior = new int[adjacencyList.size()];

		int menor = 0;
		int Vmin = 0;
		conjunto.add((int) v1);
		//inicializamos el array de distancias y el array de vé rtices anteriores.
		for (V i : adjacencyList.keySet()) {
			anterior[(int) i - 1] = (int) v1;
			if (adjacencyList.get(v1).contains(i)) {
				distancias[(int) i - 1] = 1;
			} else {
				distancias[(int) i - 1] = infinito;
			}
		}


		while (!conjunto.contains(v2)) {
			menor = infinito;

			//calculamos el mejor candidato a vertice intermedio.
			for (int i = 0; i < distancias.length; i++) {
				if (!conjunto.contains(i + 1) && distancias[i] < menor) {
					menor = distancias[i];
					Vmin = i + 1;
				}
			}
			//marcamos el vertice intermedio como visitado.
			conjunto.add(Vmin);

			//actualizamos la distancia de los vertices adyacentes.
			for (V i : adjacencyList.get(Vmin)) {
				if (menor < distancias[(int) i - 1]) {
					distancias[(int) i - 1] = menor + 1;
					anterior[(int) i - 1] = Vmin;
				}
			}
		}

		int a = (int) v1;
		int b = (int) v2;
		/*
		 * recorremos el array anterior para obtener el camino má s corto,
		 * metiendolo en una pila.
		 */
		while (a != b) {
			pila.push(b);
			b = anterior[b - 1];
		}

		//metemos el vé rtice origen en la pila.
		pila.push(a);

		//metemos el camino en la lista solución.
		while(!pila.isEmpty()){
			solucion.add(pila.peek());
			pila.pop();
		}

		return solucion;
	}


}
