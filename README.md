# **Prácticas:**


## **Autores:**

* Anónimo. (Creador de la plantilla).
* Carlos Moragón Corella.


## **Práctica 1:** Generador del número Pi através del método Montecarlo:


### Descripción:

* El programa es una representación virtual del método de Montecarlo, para calcular el número Pi.  
Este método se basa en el lanzamiento de dardos a una circunferencia inscrita en un cuadrado, lo que representaria la diana.  
Pi seria la probabilidad de que los dardos cayera en la diana. Pi = (4 x Dardos que entran en la diana) / número total de dardos lanzados.

### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis:  
	- *make jar*  
	- *java -jar ap-personas.jar* **número**  
* **Ejemplo:**  
	- make jar  
	- java -jar ap-personas.jar 10000

### Información acerca de la estructura del programa:

	- El código cuenta con 2 clases, la clase Principal y la clase Matematicas, 
	metidas en los paquetes aplicacion y mates, respectivamente.
    
### Diagrama:

![Diagrama](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama1.png)



## **Práctica 2:** Generador del número Pi através del método Montecarlo, de forma recursiva:


### Descripción:

* El programa es una representación virtual del método de Montecarlo, para calcular el número pi, de forma recursiva.
Este método se basa en el lanzamiento de dardos a una circunferencia inscrita en un cuadrado, lo que representaria una diana.
Pi seria la probabilidad de que los dardos cayera en la diana. Pi = (4 x dardos que entran en la diana) / número total de dardos lanzados.

### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis:
        - *make jar*
        - *java -jar ap-personas.jar* **número**
        (El número debe de ser menor a 10000, dado que sino se desborda de la pila).
* **Ejemplo:**
        - make jar
        - java -jar ap-personas.jar 10000

### Información acerca de la estructura del programa:

        - El código cuenta con 2 clases, la clase Principal y la clase Matematicas,
        metidas en los paquetes aplicacion y mates, respectivamente.
        
### Diagrama:

![Diagrama](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama3.a.png)



## **Práctica 3 Primera versión:** Generador del número Pi através del método Montecarlo, haciendo uso de interfaces funcionales y expresiones lambda:


### Descripción:

* El programa es una representación virtual del método de Montecarlo, para calcular el número pi, haciendo uso de interfaces funcionales.
Este método se basa en el lanzamiento de dardos a una circunferencia inscrita en un cuadrado, lo que representaria una diana.
Pi seria la probabilidad de que los dardos cayera en la diana. Pi = (4 x dardos que entran en la diana) / número total de dardos lanzados.

### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis:
        - *make jar*
        - *java -jar ap-personas.jar* **número**
        (El número debe de ser menor a 10000, dado que sino se desborda de la pila).
* **Ejemplo:**
        - make jar
        - java -jar ap-personas.jar 10000

### Información acerca de la estructura del programa:

        - El código cuenta con 2 clases, la clase Principal y la clase Matematicas,
        metidas en los paquetes aplicacion y mates, respectivamente.
        
### Diagrama:

![Diagrama3a](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama2.png)



## **Práctica 3 Versión simplificada:** Generador del número Pi através del método Montecarlo, haciendo uso de Streams y de expresiones lambda:


### Descripción:

* El programa es una representación virtual del método de Montecarlo, para calcular el número pi, haciendo uso de Streams y de expresiones lambda
Este método se basa en el lanzamiento de dardos a una circunferencia inscrita en un cuadrado, lo que representaria una diana.
Pi seria la probabilidad de que los dardos cayera en la diana. Pi = (4 x dardos que entran en la diana) / número total de dardos lanzados.

### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis:
        - *make jar*
        - *java -jar ap-personas.jar* **número**
        (El número debe de ser menor a 10000, dado que sino se desborda de la pila).
* **Ejemplo:**
        - make jar
        - java -jar ap-personas.jar 10000

### Información acerca de la estructura del programa:

        - El código cuenta con 2 clases, la clase Principal y la clase Matematicas,
        metidas en los paquetes aplicacion y mates, respectivamente.
        
### Diagrama:

![Diagramab](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama3.b.png)



## **Práctica 4**: Distancia entre dos palabras según el método de Dan Jurafsky:


### Descripción:

* El programa implementa un algoritmo el cual devuelve la distancia mínima de cambios en una cadena de caracteres, para que sea igual a otra cadena de caracteres.
Esa distancia se calcula haciendo uso de inserciones, eliminaciones y sustituciones de caracteres, tal y como expone Dan Jurafsky.

### Diapositivas Dan Jurafsky:

[Link a las diapositivas de Dan Jurafsky](https://web.stanford.edu/class/cs124/lec/med.pdf)


### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis: 
        - *mvn package* 
Tras la ejecución de este comando aparecerá la carpeta target.
        - *java -jar target/practica4-1.0-SNAPSHOT.jar*
Para cambiar las palabras con las que se van a ejecutar, entra en la clase App, cuyo path es: ./src/main/java/com/how2java/App.java

* **Ejemplo:**
        - mvn package
        - java -jar target/practica4-1.0-SNAPSHOT.jar

### Información acerca de la estructura del programa:

        - El programa sigue una estructura maven, donde la clase que contiene el método "main" es la clase App, la cual esta en el 
        paquete com.how2java. La clase que contiene el algoritmo principal es la clase CalculadoraDistancias, también contenido en 
        el paquete com.how2java.
        El programa también cuenta con una estructura de pruebas contenidas en el siguiente path:
        ./src/test/java/com/how2java/CalculadoraDistanciasTest.java.

![treePrograma](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/treePractica4.png)

        
### Diagrama:

![DiagramaP4](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/practica4.png)



## **Práctica 5**: Camino más corto entre vértices:


### Descripción:

* El programa implementa un algoritmo el cual devuelve el camino mínimo entre dos vértices de un grafo.
Este camino se calcula con una implementación del algoritmo de Dijsktra, en el cual hacemos uso de una colección Map,
para representar el grafo, y un conjunto para saber que vértices intermedios se han probado anteriormente.

El algoritmo de Dijkstra es una algoritmo voraz, lo que significa que siempre escoje la mejor opción de todas,
por lo que, en este caso, recorre en grafo cogiendo siempre el mejor vertice intermedio.

Tras haber hallado el mejor camino, hace una llamada a un método recursivo, que va añadiendo los vertices a la lista solución.


### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis: 
        - *mvn package* 
Tras la ejecución de este comando aparecerá la carpeta target.
        - *java -jar target/practica5-1.0-SNAPSHOT.jar*
Para cambiar los vértice con los que se van a ejecutar, entra en la clase App, cuyo path es: ./src/main/java/com/how2java/App.java

* **Ejemplo:**
        - mvn package
        - java -jar target/practica5-1.0-SNAPSHOT.jar
        
* Si solo quieres comprobar que pasa los test, puedes ejecutar el comando:
        - mvn test

### Información acerca de la estructura del programa:

        - El programa sigue una estructura maven, donde la clase que contiene el método "main" es la clase App, la cual esta en el 
        paquete pr2.org. La clase que contiene el algoritmo principal es la clase Graph, también contenido en 
        el paquete pr2.org.
        El programa también cuenta con una estructura de pruebas contenidas en el siguiente path:
        ./src/test/java/pr2/org/GraphTest.java.

![treePrograma](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/treePractica5b.png.jpg)

        
### Diagrama:

![DiagramaP5](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama_practica5.png)


## **Práctica 5 Iterativa**: Camino más corto entre vértices:


### Descripción:

* El programa implementa un algoritmo el cual devuelve el camino mínimo entre dos vértices de un grafo.
Este camino se calcula con una implementación del algoritmo de Dijsktra, en el cual hacemos uso de una colección Map,
para representar el grafo, y un conjunto para saber que vértices intermedios se han probado anteriormente.

El algoritmo de Dijkstra es una algoritmo voraz, lo que significa que siempre escoje la mejor opción de todas,
por lo que, en este caso, recorre en grafo cogiendo siempre el mejor vertice intermedio.

Tras haber hallado el mejor camino, utiliza un bucle while para recorrerlo, añadiendo los vertices a la lista solución.


### Ejecución del programa:

* Para ejecutar el programa debes de seguir la siguiente sintaxis: 
        - *mvn package* 
Tras la ejecución de este comando aparecerá la carpeta target.
        - *java -jar target/practica5-1.0-SNAPSHOT.jar*
Para cambiar los vértice con los que se van a ejecutar, entra en la clase App, cuyo path es: ./src/main/java/com/how2java/App.java

* **Ejemplo:**
        - mvn package
        - java -jar target/practica5-1.0-SNAPSHOT.jar
        
* Si solo quieres comprobar que pasa los test, puedes ejecutar el comando:
        - mvn test

### Información acerca de la estructura del programa:

        - El programa sigue una estructura maven, donde la clase que contiene el método "main" es la clase App, la cual esta en el 
        paquete pr2.org. La clase que contiene el algoritmo principal es la clase Graph, también contenido en 
        el paquete pr2.org.
        El programa también cuenta con una estructura de pruebas contenidas en el siguiente path:
        ./src/test/java/pr2/org/GraphTest.java.

![treePrograma](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/treePractica5b.png.jpg)

        
### Diagrama:

![DiagramaP5.iterativa](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/diagrama_practica5_iterativa.png)



## **Licencias:**


### Licencia apache:

![apache](https://bitbucket.org/cmoragon/repositoriopracticas/downloads/by_petit.png)

### Texto licencia apache:

Copyright [2022] [Carlos Moragón Corella]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the
License.


