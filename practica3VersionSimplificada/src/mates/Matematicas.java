package mates;

import java.lang.Math;
import java.util.stream.Stream;

/*
Copyright [2022] [Carlos Moragón Corella]

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

/**
 * @version java 11.0.12 2021-07-20 LTS
 *
 * @author Carlos Moragon Corella.
 *
 * @see en esta clase se realiza el método de montecarlo, mediante el uso de Streams y expresiones lambda.
 */

public class Matematicas{
	/**
	 * @param al metodo se le pasa un parametro de tipo long, el cual le damos el nombre de 'pasos' dicho parametro es el numero total de 'dardos' que se analizan.
	 *
	 * @return el método generarNumeroPiIterativo() devuelve un double, el cual se debe aproximar al numero pi: 3,1416...
	 */
	public static double generarNumeroPiIterativo(long pasos){
		return Stream.generate(() -> Math.pow(Math.random(), 2) + Math.pow(Math.random(), 2)).limit(pasos).filter(x -> x <= 1).count() * 4 /(double) pasos;
	}
}
