package pr2.org;

/*
Copyright [2022] [Carlos Moragón Corella]

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/
public class App {
	/**
	 *
	 * @param args parametro que le pasa por consola
	 * @throws Exception
	 */
	public static void main( String[] args ) throws Exception {
		// We build the graph

		Graph<Integer> g = new Graph<>();
		g.addVertex(1);
		g.addVertex(2);
		g.addVertex(3);
		g.addVertex(4);
		g.addVertex(5);

		System.out.println(g.addEdge(1, 2));
		g.addEdge(1, 5);
		g.addEdge(2, 3);
		g.addEdge(3, 4);
		g.addEdge(5, 4);

		System.out.println(g.shortestPath(1,3));

	}
}
