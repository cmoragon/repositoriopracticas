package pr2.org;

import java.util.*;
import java.util.stream.Collectors;

/*
 * Copyright [2022] [Carlos Moragón Corella]

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,

software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

public class Graph<V> {

    //Lista de adyacencia.
    private Map<V, Set<V>> adjacencyList = new HashMap<>();

    //Conjunto de vertices intermedios.
    Set<Integer> conjunto = new HashSet<Integer>();

    private final int infinito = 2147483647;

    /**
     * Añ ade el vé rtice ` v` al grafo.
     * <p>
     * 4
     *
     * @ param v vé rtice a añ adir.
     * @ return ` true` si no estaba anteriormente y ` false` en caso
     * contrario.
     */
    public boolean addVertex(V v) {
        if (!adjacencyList.containsKey(v)) {
            adjacencyList.put(v, new HashSet<V>());
            return true;
        } else return false;
    }


    /**
     * Añ ade un arco entre los vé rtices ` v1` y ` v2` al grafo. En
     * caso de que no exista alguno de los vé rtices, lo añ ade
     * tambié n.
     *
     * @ param v1 el origen del arco.
     * @ param v2 el destino del arco.
     * @ return ` true` si no existí a el arco y ` false` en caso contrario.
     */
    public boolean addEdge(V v1, V v2) {
        if (!adjacencyList.get(v1).contains(v2) && !adjacencyList.get(v2).contains(v1)) {
            adjacencyList.get(v1).add(v2);
            adjacencyList.get(v2).add(v1);
            return true;
        } else return false;
    }


    /**
     * Obtiene el conjunto de vé rtices adyacentes a ` v`.
     *
     * @ param v vé rtice del que se obtienen los adyacentes.
     * @ return conjunto de vé rtices adyacentes.
     */
    public Set<V> obtainAdjacents(V v) throws Exception {
        return adjacencyList.get(v);
    }


    /**
     * Comprueba si el grafo contiene el vé rtice dado.
     *
     * @ param v vé rtice para el que se realiza la comprobació n.
     * @ return ` true` si ` v` es un vé rtice del grafo.
     */
    public boolean containsVertex(V v) {
        return adjacencyList.keySet().contains(v);
    }


    /**
     * Mé todo ` toString()` reescrito para la clase ` Grafo. java`.
     *
     * @ return una cadena de caracteres con la lista de
     * adyacencia.
     */

    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        for (V v : adjacencyList.keySet()) {
            mensaje.append(v.toString() + ": ");
            for (V conect : adjacencyList.get(v)) {
                mensaje.append(conect.toString() + " ");
            }
        }
        return mensaje.toString();
    }


    /**
     * Obtiene, en caso de que exista, el camino má s corto entre
     * ` v1` y ` v2`. En caso contrario, devuelve ` null`.
     *
     * @ param v1 el vé rtice origen.
     * @ param v2 el vé rtice destino.
     * @ return lista con la secuencia de vé rtices del camino má s corto
     * entre ` v1` y ` v2`
     */


	public List<Integer> shortestPath(V v1, V v2) throws Exception {
		int menor = infinito;
		int Vmin = 0;
		int[] distancia = new int[adjacencyList.size()];


		int[] ant = new int[adjacencyList.size()];
		inicializar(distancia, ant, v1);

		while (!conjunto.contains((int) v2)) {
			menor = infinito;
			//calcular el mejor candidato a vertice intermedio
			for (int i = 0; i < distancia.length; i++) {
				if(distancia[i]<menor&&!conjunto.contains(i+1) ){
					menor = distancia[i];
					Vmin = i+1;
				}
			}

			//marcar el vertice intermedio como visitado
			conjunto.add(Vmin);

			//actualizar la distancia de los adjacentes
			for(V i: adjacencyList.get(Vmin)){
				if (menor<distancia[(int) i-1]){
					//actualizar la distancia al menor más una arista.
					distancia[(int) i-1] = menor+1;
					//actualizar el anterior
					ant[(int)i-1] = Vmin;
				}
			}
		}
		//Llamamos a construir el camino de forma recursiva.
		return construirCamino(ant,(int) v1,(int) v2);
	}


	/**
	 *
	 * @param ant array con con los vertices intermedios para llegar al cada vertice.
	 * @param a es el vertice origen.
	 * @param b es el vertice destino.
	 * @return lista con la secuencia de vé rtices del camino má s corto.
	 */

	public List<Integer> construirCamino(int[] ant, int a, int b){
		if(a == b){
			return new ArrayList<>(Arrays.asList(a));
		}else {
			List<Integer> l = construirCamino(ant, a, ant[b-1]);
			l.add(b);
			return l;
		}
	}

	/**
	 *
	 * @param distancia array con el camino minimo desde el origen hasta cada vertice.
	 * @param ant array con con los vertices intermedios para llegar al cada vertice.
	 * @param v1 vertice origen.
	 *
	 * @throws Exception
	 */

	public void inicializar(int[] distancia,int[] ant, V v1) throws Exception {
		conjunto.clear();
		conjunto.add((int) v1);
		for (int j = 0; j < adjacencyList.size(); j++) {

			ant[j] = (int) v1;
			distancia[j] = infinito;
		}
		for(V v: obtainAdjacents(v1)){
			distancia[(int) v - 1] = 1;
		}

	}

}
